/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dxf;

import esyoLib.DXF.Tdxf;
import java.io.IOException;

/**
 *
 * @author rpetit
 */
public class DXF {

    /**
     * @param args the command line arguments: 0 nom du DXF à ouvrir, 1 nom du jpg à enregistrer, 2 coeff zoom
     */
    public static void main(String[] args) {
        System.out.println("Ouverture de "+args[0]+"...");
        Tdxf dxf=null;
        try {
            dxf = new Tdxf(args[0]);
        } catch (IOException ex) {
            System.out.println(ex.getClass().getName()+":"+ex.getMessage());
        }
        System.out.println("Zoom de "+args[2]);
        dxf.setZoom(Integer.parseInt(args[2]));
        System.out.println("Enregistrement de "+args[1]+"...");
        try {
            dxf.saveToJPG(args[1]);
        } catch (IOException ex) {
            System.out.println(ex.getClass().getName()+":"+ex.getMessage());
        }
        System.out.println("Terminé.");
    }
    
}
